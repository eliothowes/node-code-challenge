# Answers

Ans1: On the console the string "2" will be logged followed by "1". This is because setTimeout will run the callback function after 100 milliseconds or as soon as possible once it is invoked from the callstack, whereas the second console.log runs immediately.


Ans2: This function is a recursive counter that will output to the console the number 10 decending to 0. The reason it counts down rather than up is the 'if' block does not resolve until 10 is reached and therefore this is the first function call that will run the console.log. Subsequently each of the previous function calls then log their values until the original function foo(0) is reached.


Ans3: The OR operator will return false only when the value on the left and right of the operator are falsey and true in all other cases. If we call foo(0), as 0 is falsey the function will log 5 to the console instead of 0.


Ans4: The output of the code is 3. As function foo() returns an anonymous function, when var bar = foo(1) is run I believe 1 is stored in the functions argument object. This means when bar(2) is called it knows the value of 'a' which is 1 and therefore returns 3. 

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Ans5: Having read around, mocha uses the done() function to test async functions to see how long they take to run.