// Server code goes here

const express = require('express')
const cors = require('cors')
const app = express()

app.use(cors())

const port = 3000

const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('./locations.db', (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the locations database.');
});


app.get('/locations/*', (req, resp) => {
  searchTerm = req.url.split('/')[2]
  // if (searchTerm.includes('%20')) {
  //   multipleTerms = searchTerm.split('%20').join(' ')
  //   console.log(multipleTerms)
  // } else {
  //   console.log(searchTerm)
  // }
    db.all(`SELECT * FROM locations WHERE name LIKE ?`, [`${searchTerm}%`], (err, rows) => {
        if (err) {
          console.error(err.message)
          resp.status(500)
          resp.end()
        } else {
          resp.json(rows)
        }
    })
})

app.listen(port, () => console.log(`GeoLocation app listening on port ${port}!`))