import React from 'react'
import '../styling/Results.css'

const Results = ({locations}) => {
    return (
        <ul className='results-container'>
            {locations.map(location => {
                return (
                <li className='list-item'>{location.name} (Lat: {location.latitude} / Long: {location.longitude})</li>
                )
            })}
        </ul>
    ) 
}

export default Results