import React, { useState, useEffect } from 'react'
import get from '../API'
import { debounce } from 'lodash'
import '../styling/Search.css'


const Search = ({setLocations}) => {

    const [searchTerm, setSearchTerm] = useState("")

    const loadResult = debounce(() => {
        if (searchTerm.length > 1) {
            get(searchTerm)
            .then(setLocations)
        } else {
            setLocations([])
        } 
    }, 1000)

    useEffect(() => {
        loadResult()
    }, [searchTerm])

    const handleInputChange = event => {
        setSearchTerm(event.target.value)
    }

    return (
       <form onSubmit={event => event.preventDefault()} className='search-form'>
           <label>Lookup a location:</label>
           <input type="text" placeholder="Enter location" value={searchTerm} onChange={handleInputChange} className='search-bar'/>
       </form>
    )
}

export default Search