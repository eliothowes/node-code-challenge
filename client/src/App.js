import React, { useState } from 'react';
import Search from './components/Search'
import Results from './components/Results'
import './styling/App.css'

const App = () => {

  const [locations, setLocations] = useState([])

  return (
    <div className='main'>
      <div className='header'>
        <h2 className='title'>Welcome to GeoSearcher</h2>
      </div>
      <Search setLocations={setLocations}/>
      <Results locations={locations}/>
    </div>
  )
}

export default App;
