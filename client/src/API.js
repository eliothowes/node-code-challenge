const baseUrl = 'http://localhost:3000'

const get = searchTerm => {
    return fetch(`${baseUrl}/locations/${searchTerm}`)
    .then(resp => resp.json())
}

export default get